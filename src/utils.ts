export const range = (n: number) => Array.from({ length: n }, (_v, i) => i);
