import { range } from "./utils.ts";

// The game grid is stored as a single flat array of Cell objects
// ordered left to right and starting with the top row.
//
// To calculate game moves, lineCoords() is used to pull out lines
// in the grid oriented according to the move direction.
// So for a left move, a horizontal line with the left tile first,
// and for a down move, a vertical line with the bottom tile first.
// This makes the core game logic in processLine() very simple:
// Just filter out empty tiles and merge adjacent tiles with the same value.
//
// Prior to each move, we calculate the next states for each
// direction (new grid and score). This is used to determine if the game
// is lost (no new moves are possible) and so we know which
// moves to allow next.

interface Tile {
  id: number
  value: number
}

type Cell = Tile | null

export type Cells = Cell[]
 
export type Move = readonly [number, number, number]

export interface GameState {
  grid: Cells,
  score: number
}


let tileId = 1;

function* processLine(line: Cells): Generator<any, void, [Tile, number]> {
  let lastTile = null;
  for (const tile of line) {
    if (tile === null) continue;
    if (tile.value === lastTile?.value) {
      yield [{ ...tile, value: tile.value * 2 }, tile.value * 2];
      lastTile = null;
    } else {
      if (lastTile !== null) yield [lastTile, 0];
      lastTile = { ...tile };
    }
  }
  if (lastTile !== null) yield [lastTile, 0];
}

export function getMoves(size: number): {[index: string]: Move} {
  // Constants used by lineCoords() to generate indexes into
  // the grid array to pull out lines orientated as required
  // for each move direction.
  // [startOffset, lineOffset, tileOffset]
  // Start at startOffset, add lineOffset to get to a new line,
  // and tileOffset to get to the next tile
  const moves = {
    LEFT: [0, size, 1],
    RIGHT: [size - 1, size, -1],
    UP: [0, 1, size],
    DOWN: [(size - 1) * size, 1, -size]
  } as const
  return moves;
}

function lineCoords(size: number, [startOffset, lineOffset, tileOffset]: Move) {
  return range(size).map((lineIdx) => {
    const lineStart = startOffset + lineIdx * lineOffset;
    return range(size).map((tileIdx) => lineStart + tileIdx * tileOffset);
  });
}

function emptyCoords(grid: Cells) {
  return grid.flatMap((tile, coord) => (tile === null ? [coord] : []));
}

function* randomEmptyCoords(grid: Cells): Generator<any, void, number> {
  let available = emptyCoords(grid);
  while (available.length > 0) {
    const idx = Math.floor(Math.random() * available.length);
    const deleted = available.splice(idx, 1);
    yield deleted[0];
  }
}

function emptyGrid(size: number) {
  return range(size * size).map((tile) => null);
}

function applyMove(size: number, move: Move, grid: Cells): GameState {
  const newGrid = emptyGrid(size);
  let score = 0;
  for (const coords of lineCoords(size, move)) {
    let newLine = processLine(coords.map((coord) => grid[coord]));
    coords.forEach((coord) => {
      const next = newLine.next();
      if (!next.done) {
        newGrid[coord] = next.value[0];
        score += next.value[1];
      }
    });
  }
  return { grid: newGrid, score };
}

function sameTile(tile1: Cell, tile2: Cell) {
  return tile1?.id === tile2?.id;
}

function calculateMoves(size: number, moves: Move[], grid: Cells): {move: Move, state: GameState}[] {
  return moves
    .map((move) => ({move, state: applyMove(size, move, grid)}))
    .filter(({move, state}) => state.grid.some(
        (tile: Cell, idx: number) => !sameTile(tile, grid[idx])
      )
    )
}

export function hasWon(grid: Cells) {
  return grid.some((tile) => tile?.value === 2048);
}

export function nextStates(size: number, moves: Move[], { grid, score }: GameState): Map<Move, GameState> {
  const states = new Map();
  for (const {move, state} of calculateMoves(size, moves, grid)) {
    addNewTiles(state.grid, 1);
    states.set(move, { grid: state.grid, score: score + state.score });
  }
  return states;
}

function addNewTiles(grid: Cells, count: number) {
  const coords = randomEmptyCoords(grid);
  for (const _ of range(count)) {
    const next = coords.next();
    if (next.done) break;
    grid[next.value] = {
      id: tileId++,
      value: Math.random() < 0.9 ? 2 : 4
    };
  }
}

function initialGrid(size: number) {
  const grid = emptyGrid(size);
  addNewTiles(grid, 2);
  return grid;
}

export function initialState(size: number): GameState {
  return {
    grid: initialGrid(size),
    score: 0
  }
}
