import { createStore, createEvent, sample, combine, guard } from "effector"
import { getMoves, hasWon, initialState, nextStates, GameState, Move } from './core.ts'

export type { Cells } from './core.ts'

export const Status = {
    playing: "PLAYING",
    won: "WON",
    lost: "LOST"
} as const

  
export function makeApi(size: number) {

    // Move constants
    const allMoves = getMoves(size)
    const moves = Object.values(allMoves)

    // Events
    const requestMove = createEvent<Move>()
    const restart = createEvent()

    // Stores current GameState
    const $currentState = createStore<GameState>(initialState(size)).reset(restart)

    // Stores possible next GameStates indexed by Move
    const $nextStates = $currentState.map(
        state => nextStates(size, moves, state)
    )

    // The Set of possible next Moves
    const $availableMoves = $nextStates.map(
        (states, moves: (Set<Move> | undefined)) => {
            const newMoves = [...states.keys()]
            // Return the current store value if the moves haven't changed to avoid unnecessary updates
            if ((moves?.size === newMoves.length) && (newMoves.every(move => moves.has(move)))) {
                return moves
            }
            return new Set(newMoves)
        }
    )

    // Derive the current game Status
    const $status = combine($currentState, $nextStates, (state: GameState, nextStates: Map<Move, GameState>) => {
        if (hasWon(state.grid)) {
            return Status.won
        }
        if (nextStates.size === 0) {
            return Status.lost
        }
        return Status.playing
    })

    // Internal event that only fires if a move has been requested and the game is playing
    const performMove = guard({
        source: requestMove,
        filter: $status.map(status => status === Status.playing)
    })

    // Switch to the next GameState on receiving the performMove event 
    sample({
        clock: performMove,
        source: [$currentState, $nextStates],
        fn: ([currentState, nextStates], move: Move) => {
            const next = nextStates.get(move)
            return (next === undefined) ? currentState : next
        },
        target: $currentState
    })

    return {
        moves: allMoves,
        restart,
        requestMove,
        $state: $currentState,
        $availableMoves,
        $status
    }
}   
