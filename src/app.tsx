import React, { useEffect } from "react"
import ReactDOM from "react-dom"
import { useSpring, animated } from "react-spring"
import { useStore } from "effector-react"

import { makeApi, Status, Cells } from "./game.ts"
import { range } from "./utils.ts"

type Coord = number[]

const COLOURS = new Map([
  [2, { fg: "#776e65", bg: "#eee4da" }],
  [4, { fg: "#776e65", bg: "#ede0c8" }],
  [8, { fg: "#f9f6f2", bg: "#f2b179" }],
  [16, { fg: "#f9f6f2", bg: "#f59563" }],
  [32, { fg: "#f9f6f2", bg: "#f67c5f" }],
  [64, { fg: "#f9f6f2", bg: "#f65e3b" }],
  [128, { fg: "#f9f6f2", bg: "#edcf72" }],
  [256, { fg: "#f9f6f2", bg: "#edcc61" }],
  [512, { fg: "#f9f6f2", bg: "#edc850" }],
  [1024, { fg: "#f9f6f2", bg: "#edc53f" }],
  [2048, { fg: "#f9f6f2", bg: "#edc22e" }]
]);

const SIZE = 4
const api = makeApi(SIZE)

const MOVES = new Map([
  ["ArrowLeft", api.moves.LEFT],
  ["ArrowRight", api.moves.RIGHT],
  ["ArrowUp", api.moves.UP],
  ["ArrowDown", api.moves.DOWN]
])


function Square(props: { r: number, pos: number, size: number, fill: string | undefined }) {
  return (
    <rect
      rx={props.r}
      ry={props.r}
      x={props.pos}
      y={props.pos}
      width={props.size}
      height={props.size}
      fill={props.fill}
    />
  );
}

function TileBackgrounds(props: { xys: Coord[] }) {
  return (
    <g>
      {props.xys.map(([x, y]) => {
        return (
          <g
            key={`${x},${y}`}
            transform={`translate(${x + 0.5},${y + 0.5}) scale(0.85)`}
          >
            <Square r={0.05} pos={-0.5} size={1} fill="#cdc1b4" />
          </g>
        );
      })}
    </g>
  );
}


function Tile(props: { value: number, x: number, y: number }) {

  const { xy, opacity } = useSpring({
    from: { opacity: 0 },
    to: { xy: [props.x + 0.5, props.y + 0.5], opacity: 1 }
  });

  return (
    // @ts-ignore
    <animated.g
      transform={xy.to((x: number, y: number) => `translate(${x},${y}) scale(0.85)`)}
      opacity={opacity.to({
        range: [0, 0.9, 1],
        output: [0, 0, 1]
      })}
    >
      <Square r={0.05} pos={-0.5} size={1} fill={COLOURS.get(props.value)?.bg} />
      <text
        x="0"
        y="0"
        fontSize="0.4"
        fontWeight="bold"
        fill={COLOURS.get(props.value)?.fg}
        dominantBaseline="middle"
        textAnchor="middle"
      >
        {props.value}
      </text>
    {/* @ts-ignore */}
    </animated.g>
  )
}

function Tiles(props: { grid: Cells, xys: Coord[] }) {
  return (
    <g>
      {props.grid.map(
        (tile, idx) =>
          tile && (
            <Tile
              key={tile.id}
              value={tile.value}
              x={props.xys[idx][0]}
              y={props.xys[idx][1]}
            />
          )
      )}
    </g>
  )
}

function App() {
  const state = useStore(api.$state)
  const status = useStore(api.$status)

  useEffect(() => {
    const onKeyDown = (event: KeyboardEvent ) => {
      const move = MOVES.get(event.key);
      if (move) api.requestMove(move);
    };
    window.addEventListener("keydown", onKeyDown);
    return () => {
      window.removeEventListener("keydown", onKeyDown);
    };
  }, []);

  // grid is a flat array of the tiles, so we create a matching array of the (x,y)
  // coords for each tile
  const xys: Coord[] = range(SIZE).flatMap((y: number) => range(SIZE).map((x: number) => [x, y]));

  const localSize = SIZE - 1 + 1.25;

  return (
    <div className="app">
      <p>
        Score {state.score}
        {(status !== Status.playing) && <span> {status === Status.won ? "WON" : "LOST"}</span>}
      </p>
      <svg viewBox={`0 0 ${localSize} ${localSize}`} width="250" height="250">
        <Square r={0.1} pos={0} size={localSize} fill="#bbada0" />
        <g transform="translate(0.125, 0.125)">
          <TileBackgrounds xys={xys} />
          <Tiles xys={xys} grid={state.grid} />
        </g>
      </svg>
      <button onClick={() => api.restart()}>
        Restart
      </button>
      <p>
        A version of <a href="https://gabrielecirulli.github.io/2048/">2048</a>{" "}
        implemented using <a href="https://reactjs.org/">React</a>
        <br />
        with <a href="https://effector.dev/">effector.js</a> for state management
        <br />
        and <a href="https://react-spring.io/">react-spring</a> for animation.
        <br />
        <br />
        More info&nbsp;
        <a href="https://www.codesimple.net/blog/2048-effectorjs-typescript-deno/">
          on my blog
        </a>.
      </p>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
