#!/bin/bash
deno bundle --unstable --watch --config 'deno.json' --import-map 'src/import_map.json' 'src/app.tsx' 'public/gen/bundle.js' &
bundle_pid=$!
trap 'kill $bundle_pid' EXIT
deno run --allow-net --allow-read 'https://deno.land/std@0.106.0/http/file_server.ts' 'public' --port '8000'
